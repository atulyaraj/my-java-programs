// import java.util.StringBuilder;

class Mutation{

    public static void main(String[] args){

        // Immutable
        String str = "Unleash";

        System.out.println(str);

        str = str.concat("your potential");

        System.out.println(str);


        // Mutable

        StringBuilder strB = new StringBuilder("This is builder");

        System.out.println(strB);

        strB.append("Yes it is");

        System.out.println(strB);

        

    }
}



