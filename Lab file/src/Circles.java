
public class Circles {
	
	Circles(float radius){
		
		this.radius = radius;
	}
	
	final double PI = 3.14159;
	
	float radius;
	
	void circumference() {
		System.out.print("Circumference: ");
		System.out.println(2 * PI * this.radius);
	}
	
	void area() {
		System.out.print("Area: ");
		System.out.println(PI * this.radius * this.radius);
	}

	public static void main(String[] args) {
		
		Circles circles = new Circles(5);
		circles.area();
		circles.circumference();
		

	}

}
