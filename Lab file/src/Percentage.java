import java.util.Scanner;


public class Percentage {
	
	Scanner cin = new Scanner(System.in);

    int marks[] = new int[5];
    float per;

    void get(){
        
        System.out.println("Enter the marks of student:");

        for(int i = 0 ; i < 5; i++){
            marks[i] = cin.nextInt();
        }
    }

    void put(){

        for(int i = 0 ; i < 5; i++){
            System.out.println(marks[i]);
        }

        System.out.print("Your percentage is: ");
        System.out.print(per);
        System.out.print("%");

    }

    void percentage(){

        int total = 0;

        for(int i = 0 ; i < 5; i++){
            total += marks[i];
        }
        
        System.out.println(total);
        
        per = (total / 5);
  
    }


	public static void main(String[] args) {
		
		
		Percentage p = new Percentage();
		
		p.get();
		p.percentage();
		p.put();

	}

}
