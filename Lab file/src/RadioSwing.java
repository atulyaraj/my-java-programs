import javax.swing.*;
import java.awt.event.*;
public class RadioSwing {

	JFrame f;

	RadioSwing() {
		f = new JFrame();
		f.setSize(300, 500);
		f.setLayout(null);
		f.setVisible(true);
		
		
		JRadioButton maleRadio = new JRadioButton("A) Male");
		maleRadio.setBounds(75, 50, 100, 30);

		
		JRadioButton femaleRadio = new JRadioButton("B) Female");
		femaleRadio.setBounds(75, 80, 100, 30);
		
		JRadioButton nonBiRadio = new JRadioButton("C) Non Binary");
		nonBiRadio.setBounds(75, 110, 120, 30);
		
		JTextArea quoteArea = new JTextArea();
		quoteArea.setBounds(0, 200, 300, 300);
		
		
		JButton submitButton = new JButton("Submit");
		submitButton.setBounds(75, 170, 85, 20);
		submitButton.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent ae) {
				
				if(maleRadio.isSelected()) {
					quoteArea.setText("The greatest deception men suffer is from\n"
							+ "their own opinions.\n"
							+ "Leonardo da Vinci");
				}
				
				if(femaleRadio.isSelected()) {
					quoteArea.setText("\"There are two powers in the world; \n"
							+ "one is the sword and the other is the pen. \n"
							+ "There is a third power stronger than both, \n"
							+ "that of women.\" \n"
							+ "— Malala Yousafzai");
				}
				
				if(nonBiRadio.isSelected()) {
					quoteArea.setText("If you try presenting yourself as \n"
							+ "something other than your birth assigned \n"
							+ "gender,mand it makes you feel euphoric, \n "
							+ "that’s just as valid a reason to transition \n"
							+ "as escaping dysphoria. \n"
							+ "– Laura Kate Dale, 2021");
				}
			}
		});
		
		
		
		ButtonGroup bg = new ButtonGroup();
		bg.add(maleRadio);
		bg.add(femaleRadio);
		bg.add(nonBiRadio);
		
		f.add(maleRadio);
		f.add(femaleRadio);
		f.add(nonBiRadio);
		f.add(submitButton);
		f.add(quoteArea);
		
	}

	public static void main(String[] args) {

		new RadioSwing();

	}

}
