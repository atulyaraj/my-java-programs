import java.io.*;



public class SerializationToFile {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		try{
			
			FileOutputStream fos = new FileOutputStream("object.dat");
		     ObjectOutputStream oos = new ObjectOutputStream(fos);

			    // create a new user object
			    User user = new User("John Doe", "john.doe@example.com",
			            new String[]{"Member", "Admin"}, true);

			    // write object to file
			    oos.writeObject(user);

			} catch (IOException ex) {
			    ex.printStackTrace();
			}

	}

}


class User implements Serializable {

    public String name;
    public String email;
    private String[] roles;
    private boolean admin;

    public User() {
    }

    public User(String name, String email, String[] roles, boolean admin) {
        this.name = name;
        this.email = email;
        this.roles = roles;
        this.admin = admin;
    }

    
}