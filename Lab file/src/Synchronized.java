
public class Synchronized extends Thread{
	
	int amount = 0 ;
	
	public void run() {
		
		for(int i = 0 ; i < 11; i++) {
			amount++;
		}	
	}
	
	void show() {
		System.out.println(amount);
	}
	
	

	public static void main(String[] args) {

		Synchronized syn = new Synchronized();
		Synchronized syn2 = new Synchronized();
		
		syn.start();
		
		syn2.start();
		
		

		syn.show();
		syn2.show();
	}

}
