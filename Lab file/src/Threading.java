
public class Threading extends Thread{
	
	int id;
	
	Threading(int id){
		this.id = id;
	}
	
	public void run() {
		
		while(true) {
			System.out.print("Running: ");
			System.out.println(this.id);
			}
		
	}

	public static void main(String[] args) {
		
		Threading myThread = new Threading(1);
		
		Threading myThread2 = new Threading(2);
		
		myThread.start();
		myThread2.start();
		
		System.out.print("Main method");
		

	}

}
