import javax.swing.*;
import java.awt.event.*;

public class AdderSwing {
	
	AdderSwing(){
		
		//Frame
				JFrame frame = new JFrame("Adder");
				frame.setSize(400, 400);
				frame.setVisible(true);
				frame.setLayout(null);
				
				
				//Label- Number 1
				JLabel num1Lbl = new JLabel("Number one: ");
				num1Lbl.setBounds(50, 20, 100, 100);
				
				
				//Label- Number 2
				JLabel num2Lbl = new JLabel("Number two: ");
				num2Lbl.setBounds(50, 60, 100, 100);
				
				//Label- result
				JLabel resultLbl = new JLabel("Result: ");
				resultLbl.setBounds(90, 100, 100, 100);
				
				
				
				//Textfield for number 1
				JTextField num1Input = new JTextField();
				num1Input.setBounds(140, 60, 100, 20);
				
				
				
				
				//Textfield for number 2
				JTextField num2Input = new JTextField();
				num2Input.setBounds(140, 100, 100, 20);
				
				
				//Textfield for result
				JTextField tfResult = new JTextField();
				tfResult.setBounds(140, 140, 100, 20);
				


				JButton addButton = new JButton("Add");
				addButton.setBounds(150, 200, 100, 50);
				addButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						
						String s1 = num1Input.getText();  
				        String s2 = num2Input.getText();  
				        
				        int a = Integer.parseInt(s1);  
				        int b = Integer.parseInt(s2);  
				        int c = 0;
				        
				        if(e.getSource() == addButton){  
				            c = a + b;  
				        }
				        
				        String result = String.valueOf(c);  
				        tfResult.setText(result);  
				    }

				});
				
				frame.add(num1Lbl);
				frame.add(num2Lbl);
				frame.add(resultLbl);
				frame.add(num1Input);
				frame.add(num2Input);
				frame.add(tfResult);
				frame.add(addButton);
	}

	public static void main(String[] args) {

			AdderSwing adder = new AdderSwing();
		

	}

}
