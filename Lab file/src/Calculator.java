import javax.swing.*;
import java.awt.FlowLayout;
import java.awt.event.*;


class Calculator extends JFrame implements ActionListener {


    JFrame frame;
    JLabel lblNo1, lblNo2, lblAns;
    JTextField tfNo1, tfNo2, tfAns;
    JButton addBtn, subBtn, mulBtn, divBtn;


    Calculator(){

        frame = new JFrame("Calculator");

        tfNo1 = new JTextField(15);

        tfNo2 = new JTextField(15);

        tfAns = new JTextField(15);

        lblNo1 = new JLabel("Number 1: ");
    
        lblNo2 = new JLabel("Number 2: ");

        lblAns = new JLabel("Result: ");

        addBtn = new JButton("Addition");
        addBtn.addActionListener(this);

        subBtn = new JButton("Subtract");
        subBtn.addActionListener(this);

        mulBtn = new JButton("Multiply");
        mulBtn.addActionListener(this);

        divBtn = new JButton("Division");
        divBtn.addActionListener(this);

        //FlowLayout fl = 

        frame.setLayout(new FlowLayout());

        frame.add(lblNo1);
        frame.add(tfNo1);

        frame.add(lblNo2);
        frame.add(tfNo2);

        frame.add(lblAns);
        frame.add(tfAns);

        frame.add(addBtn);
        frame.add(subBtn);
        frame.add(mulBtn);
        frame.add(divBtn);

        frame.setSize(300, 300);

        frame.setVisible(true);


    }

    // Class without new??

    public void actionPerformed(ActionEvent e){

        int no1 = Integer.parseInt(tfNo1.getText());
        int no2 = Integer.parseInt(tfNo2.getText());
        int ans = 0;

        if(tfNo1.getText() == ""){

            tfNo1.grabFocus();

        }else if(tfNo2.getText() == ""){

            tfNo2.grabFocus();

        }else{

            //JButton clicked = e.getSource();

        if(e.getSource() == addBtn){

            ans = no1 + no2;

        }else if(e.getSource() == subBtn){

            ans = no1 - no2;

        }else if(e.getSource() == mulBtn){

            ans = no1 * no2;

        }else if(e.getSource() == divBtn){

            ans = no1 / no2;
        }

        }

        tfAns.setText(Integer.toString(ans));

    }

    public static void main(String[] a){
        Calculator c1 = new Calculator();
    }
}