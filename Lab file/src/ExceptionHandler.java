import java.util.Scanner;


public class ExceptionHandler {

	public static void main(String[] args) throws Exception {
		
		try {
			int array[] = new int[5];
			
			array[6]  = 9;
			
		}catch(ArrayIndexOutOfBoundsException e) {
			
			System.out.println("Access wrong element of array");
			
		}
		
		
		try {
			int num = 5 / 0;
		}
		catch(ArithmeticException e) {
			System.out.println("Dividing by zero");
		}
		
		
		Scanner cin = new Scanner(System.in);
		
		System.out.print("Enter your age:");
		int age = cin.nextInt();
			
			if(age > 30) {
				throw new Exception("Only under 30 are allowed, Sorry.");
			}
		
		
	}

}
