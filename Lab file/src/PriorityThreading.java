
public class PriorityThreading extends Thread{
	
	int id;
	
	PriorityThreading(int id){
		this.id = id;
	}
	
	public void run() {
		for(int i = 0 ; i < 1000; i++) {
			System.out.println( this.id + " thread working");
		}
	}

	public static void main(String[] args) {
		
		PriorityThreading p1 = new PriorityThreading(1); 
		PriorityThreading p2 = new PriorityThreading(2); 
		PriorityThreading p3 = new PriorityThreading(3); 
		PriorityThreading p4 = new PriorityThreading(4); 
		
		p1.setPriority(MAX_PRIORITY);
		p2.setPriority(MIN_PRIORITY);
		p3.setPriority(NORM_PRIORITY);
		p4.setPriority(MAX_PRIORITY);
		
		
		
		p1.start();
		p2.start();
		p3.start();
		p4.start();

	}

}
