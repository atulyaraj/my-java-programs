class Shape{
    int length;
    int breadth;
    void area(){
        System.out.println("Area");
    }
}
class Square extends Shape{
    Square(int length){
        this.length = length;
    }
    void area(){System.out.println("Area of Square: " + length * length);}
}
class Rectangle extends Shape{
    Rectangle(int length, int breadth){
        this.length = length;
        this.breadth = breadth;
    }
    void area(){
        System.out.println("Area of rectangle: " + this.length * this.breadth );
    }
}
class Triangle extends Shape{
    int base;
    Triangle(int base, int length){
        this.base = base;
        this.length = length;
    }
    void area(){

        System.out.print("Area of triangle: ");
        System.out.println((this.length * this.base) * 1/2 );}
}
class SolutionArea{
    public static void main(String [] args){
        Square square = new Square(4);
        square.area();
        Rectangle rectangle = new Rectangle(2, 4);
        rectangle.area();
        Triangle triangle = new Triangle(5, 10);
        triangle.area();
    }
}