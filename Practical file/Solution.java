import java.util.Scanner;
import java.util.Random;

class AccountHelper{
    Scanner cin  = new Scanner(System.in);
    Random random = new Random();
}

class Account extends AccountHelper{
    Account(){
        this.accNumber = this.random.nextInt((50000 - 10000) + 1) + 10000;
    }

    String name;
    String dateOfBirth;
    int accNumber;
    int balance;
    String accType;

    public void createAccount(){
        System.out.println("Welcome to Atulya Bank");
        System.out.println("Please enter your good name: ");
        this.name = this.cin.nextLine();
        System.out.println("Please enter you date of birth (DD-MM-YYYY): ");
        this.dateOfBirth = cin.nextLine();
    }

    public void withdrawl(int amount){
        this.balance -= amount;
    }

    public void deposit(int amount){
        this.balance += amount;
    }

    public void statement(){
        System.out.println("\n\nAtulya bank statement:");
        System.out.println("Name: " + this.name);
        System.out.println("Account number: " + this.accNumber);
        System.out.println("Current Balance: " + this.balance);     
    }
}

class Current extends Account{
    Current(){
        this.accType = "Current";
    }

    int minimumAmount = 10000;

    public void createAccount(){
        super.createAccount();
        System.out.println("Your current account have been created!");
        System.out.println("Please deposit amount $10,000, to fulfill minimum requirements");
        int amount;
        System.out.print("Enter amount: ");
        amount = cin.nextInt();
        if(amount < 10000){
            System.out.println("The amount is less than $10,000!");
            return;
        }else{
            deposit(amount);
        }
    }

    public void statement(){
        super.statement();
        System.out.println("Account type: " + this.accType + "\n\n");

    }

    public void withdrawl(int amount){

        if(this.balance <= 10000){
            System.out.println("Insuffienceint balance!");
            return;
        }else{
            super.withdrawl(amount);
        }
    }   
}

class Saving extends Account{
    int minimumAmount = 500;
    int transactionsLimits = 200;
    int transactions;

    Saving(){
        this.accType = "Saving";
    }

    public void createAccount(){
        super.createAccount();
        System.out.println("Your saving account have been created!");
        System.out.println("Please deposit amount $500, to fulfill minimum requirements");
        int amount;
        System.out.print("Enter amount: ");
        amount = cin.nextInt();
        if(amount < 500){
            System.out.println("The amount is less than $500!");
            return;
        }else{
            deposit(amount);
        }
    }

    public void statement(){
        super.statement();
        System.out.println("Account type: " + this.accType + "\n\n");

    }

    public void withdrawl(int amount){

        if(this.balance <= 500){
            System.out.println("Insuffienceint balance!");
            return;
        }else{
            super.withdrawl(amount);
        }
    }

    public boolean isTransactionlimitReached(){

        if(this.transactions > 200){
            System.out.println("Transaction limit reached!");
            char input;
            do{
                System.out.println("Renew trasaction limit? y/n: ");
                input = cin.next().charAt(0);
                if(input == 'y'){
                    this.transactions = 0;

                }else if(input == 'n'){
                    return true;
                }else{
                    System.out.println("Enter: y, n, 0 only!");
                }
                
            }while(input != 'n');

            return true;

        }else{
            return false;
        }
    }
    public void deposit(int amount){
        this.transactions++;
        isTransactionlimitReached();
        super.deposit(amount);        
    }
}

class Solution{

    public static void main(String[] args){

        Current atul = new Current();
        atul.createAccount();
        atul.deposit(200);
        atul.statement();
        atul.withdrawl(100);
        atul.statement();

        Saving atulKaSavingAccount = new Saving();
        atulKaSavingAccount.createAccount();
        atulKaSavingAccount.deposit(1000);
        atulKaSavingAccount.statement();
        atulKaSavingAccount.statement();
        atulKaSavingAccount.withdrawl(500);
        atulKaSavingAccount.statement(); 
    }
}