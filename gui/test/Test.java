import java.awt.*;
import java.applet.*;

class Test extends Frame{

    Frame frame;
        TextField txt_1, txt_2, txt_3;
        Label label_1, label_2, label_3;
        Button button_add;

    Test(){

        frame = new Frame("My first program");

        txt_1 = new TextField(20);

        txt_2 = new TextField(20);

        txt_3 = new TextField(20);

        label_1 = new Label("Number 1: ");
    
        label_2 = new Label("Number 2: ");

        label_3 = new Label("Result: ");

        button_add = new Button("ADD");

        FlowLayout fl = new FlowLayout();

        frame.setLayout(fl);

        frame.add(label_1);
        frame.add(txt_1);

        frame.add(label_2);
        frame.add(txt_2);

        frame.add(label_3);
        frame.add(txt_3);

        frame.add(button_add);

        frame.setSize(600, 400);

        frame.setVisible(true);

    }

    public static void main(String[] args){
        Test t1 = new Test();
            }
}