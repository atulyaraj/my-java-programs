import java.awt.*;
import java.applet.*;
import javax.swing.*;

class Test extends JFrame{

    JFrame frame;
        JTextField txt_1, txt_2, txt_3;
        JLabel label_1, label_2, label_3;
        JButton button_add;

    Test(){

        frame = new JFrame("My first program");

        txt_1 = new JTextField(20);

        txt_2 = new JTextField(20);

        txt_3 = new JTextField(20);

        label_1 = new JLabel("Number 1: ");
    
        label_2 = new JLabel("Number 2: ");

        label_3 = new JLabel("Result: ");

        button_add = new JButton("ADD");

        FlowLayout fl = new FlowLayout();

        frame.setLayout(fl);

        frame.add(label_1);
        frame.add(txt_1);

        frame.add(label_2);
        frame.add(txt_2);

        frame.add(label_3);
        frame.add(txt_3);

        frame.add(button_add);

        frame.setSize(600, 400);

        frame.setVisible(true);

    }

    public static void main(String[] args){
        Test t1 = new Test();
        }
}