import java.awt.*;
import java.awt.event.*;


class Calculator extends Frame implements ActionListener {


    Frame frame;
    Label lblNo1, lblNo2, lblAns;
    TextField tfNo1, tfNo2, tfAns;
    Button addBtn, subBtn, mulBtn, divBtn;


    Calculator(){

        frame = new Frame("Calculator");

        tfNo1 = new TextField(20);

        tfNo2 = new TextField(20);

        tfAns = new TextField(20);

        lblNo1 = new Label("Number 1: ");
    
        lblNo2 = new Label("Number 2: ");

        lblAns = new Label("Result: ");

        addBtn = new Button("Addition");
        addBtn.addActionListener(this);

        subBtn = new Button("Subtract");
        subBtn.addActionListener(this);

        mulBtn = new Button("Multiply");
        mulBtn.addActionListener(this);

        divBtn = new Button("Division");
        divBtn.addActionListener(this);

        FlowLayout fl = new FlowLayout();

        frame.setLayout(fl);

        frame.add(lblNo1);
        frame.add(tfNo1);

        frame.add(lblNo2);
        frame.add(tfNo2);

        frame.add(lblAns);
        frame.add(tfAns);

        frame.add(addBtn);
        frame.add(subBtn);
        frame.add(mulBtn);
        frame.add(divBtn);

        frame.setSize(600, 400);

        frame.setVisible(true);


    }

    // Class without new??

    public void actionPerformed(ActionEvent e){

        int no1 = Integer.parseInt(tfNo1.getText());
        int no2 = Integer.parseInt(tfNo2.getText());
        int ans;

        if(tfNo1.getText() == ""){

            tfNo1.focus();

        }else if(tfNo2.getText() == ""){

            tfNo2.focus();

        }else{

            //Button clicked = e.getSource();

        if(e.getSource() == addBtn){

            ans = no1 + no2;

        }else if(e.getSource() == subBtn){

            ans = no1 - no2;

        }else if(e.getSource() == mulBtn){

            ans = no1 * no2;

        }else if(e.getSource() == divBtn){

            ans = no1 / no2;
        }

        }

        tfAns.setText(ans);

    }

    public static void main(String[] a){
        Calculator c1 = new Calculator();
    }
}