import java.util.ArrayList; 

abstract class Room{

    int length;
    int breadth;
    int height;

    // int numWalls;

    // String[] colorWalls;

     ArrayList<String> decorationItems = new ArrayList<String>();

    int windows;

    int doors;

    boolean balconyAvailable;

    boolean bathroomAttached;

    public void area(){
        System.out.println("Area of room: " + (length * breadth) + " metre square");
    }

    public void decorate(String items){
        decorationItems.add(items);
    }

    public void showDimentions(){
        System.out.println("Length of room: " + this.length + " metre");
        System.out.println("Breadth of room: " + this.breadth + " metre");
        System.out.println("Height of room: " + this.height + " metre");

    }

    public void showDecorations(){
        for(int i = 0 ; i < this.decorationItems.size(); i++){
           System.out.println("Decoration item: " + this.decorationItems.get(i));
 
        }
    }
}

class Bedroom extends Room{

    Bedroom(){
        super.length = 10;
        super.breadth = 10;
        super.height = 10;

    }

    Bedroom(int length, int breadth, int height){
            super.length = length;
            super.breadth = breadth;
            super.height = height;
    }  
}


class Program4{
    public static void main(String[] args){

        Bedroom myBedroom = new Bedroom();

        myBedroom.showDimentions();
        myBedroom.area();

        myBedroom.decorate("Dog Image");
        myBedroom.decorate("Bedside lamp");

        myBedroom.showDecorations();

        Bedroom myBedroom2 = new Bedroom(12, 14, 9);

        myBedroom2.showDimentions();
        myBedroom2.area();

    }
}