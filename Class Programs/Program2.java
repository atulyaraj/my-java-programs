class Queue{

    public void initialize(){
        System.out.println("Initializing queue..");
    }
    
}

class CircularQueue extends Queue{

    public void initialize(){
        //super.initialize();
        System.out.println("Initializing circular queue..");
    }

}

class Program2{
    public static void main(String[] args){
        CircularQueue cq = new CircularQueue();
        cq.initialize();
    }
}