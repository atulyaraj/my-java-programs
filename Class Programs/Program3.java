interface Computer{

    public void runProgram(String program);

    public void shutdown();

    public void displayConfiguration();

}


interface Gaming{

    public void refreshRate();

    public void connectivity();
}


class Laptop implements Computer, Gaming{
    int cpuCores;
    boolean gpuAvailable;
    int storageCapacity;
    String storageType;

    Laptop( int cpuCores, boolean gpuAvailable, int storageCapacity, String storageType){
        this.cpuCores = cpuCores;
        this.gpuAvailable = gpuAvailable;
        this.storageCapacity = storageCapacity;
        this.storageType = storageType;
    }

    public void runProgram(String program){
        System.out.println("Running program: " + program);
    }

    public void shutdown(){
        System.out.println("Shutting down system!");
    }

    public void displayConfiguration(){
        System.out.println("CPU Cores: " + this.cpuCores);
        System.out.println("GPU available:" + this.gpuAvailable);
        System.out.println("Storage: " + this.storageType +" "+ this.storageCapacity);
    }

    public void refreshRate(){
        System.out.println("Refresh rate @ 60Hz");
    }

    public void connectivity(){
        System.out.println("Connectivity: Ethernet and WIFI");
    }    

}


class Program3{
    public static void main(String[] args){

        Laptop pavillion = new Laptop(12, true , 500, "SSD");

        pavillion.displayConfiguration();

        pavillion.refreshRate();

        pavillion.connectivity();

    }
}