class Parent{
    public void display(){
        System.out.println("Parent is showing");
    }
}

class Child {
    public void display(){
        System.out.println("Child is showing");
    }
}


class NoInheritance{

    public static void main(String[] args){

        Parent p1 = new Parent();

        Child c1 = new Child();

    }

    
}

