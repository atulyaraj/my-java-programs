class Parent{
    public void display(){
        System.out.println("Parent is showing");
    }
}

class Child extends Parent{
    public void display(){
        super.display();
        System.out.println("Child is showing");
    }
}


class SuperInheritance{

    public static void main(String[] args){

        Child c1 = new Child();
        c1.display();

    }
    
}


