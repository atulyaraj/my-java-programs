class Arithematix{
    static void calculation(int op1, int op2, char operator){
        switch(operator){
            case '+': System.out.print(op1 + " added to " + op2 + " is ");
                        System.out.println(op1 + op2);
            break;
            case '-': System.out.print(op1 + " subtracted from " + op2 + " is ");
                        System.out.println(op1 - op2);
            break;
            case '/': System.out.print(op1 + " divided by " + op2 + " is ");
                        System.out.println(op1 / op2);
            break;
            case 'm': System.out.print(op1 + " multiplied by " + op2 + " is ");
                         System.out.println(op1 * op2);
            break;
            case '%': System.out.print(op1 + " divided by " + op2 + " and the remainder is ");
                        System.out.println(op1 % op2);
            break;
            default: System.out.println("use only: +,-,m,%");
        }
    }
    public static void main(String[] args){

        if(args.length != 3){
            System.out.println("usa9ge: java Arithematix operand operator operand");
            System.out.println("Use 'm' instead of '*'");
            System.out.println("example: java Arithematix 2 + 2");
        }else{
            int op1 = Integer.parseInt(args[0]);
            System.out.println(args[0]);
            char operator = args[1].charAt(0);
            int op2 = Integer.parseInt(args[2]);
            
            calculation(op1, op2, operator);
        }
    }
}