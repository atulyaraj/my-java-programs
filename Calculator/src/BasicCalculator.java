import java.awt.*;
import java.awt.event.*;



class BasicCalculator extends Frame implements ActionListener {


    //private static final long serialVersionUID = 1L;
	Frame frame;
    Label lblNo1, lblNo2, lblAns;
    TextField tfNo1, tfNo2, tfAns;
    Button addBtn, subBtn, mulBtn, divBtn, exitBtn;


    BasicCalculator(){

        frame = new Frame("Calculator");

        tfNo1 = new TextField(20);

        tfNo2 = new TextField(20);

        tfAns = new TextField(20);

        lblNo1 = new Label("Number 1: ");
    
        lblNo2 = new Label("Number 2: ");

        lblAns = new Label("Result: ");

        addBtn = new Button("Addition");
        addBtn.addActionListener(this);

        subBtn = new Button("Subtract");
        subBtn.addActionListener(this);

        mulBtn = new Button("Multiply");
        mulBtn.addActionListener(this);

        divBtn = new Button("Division");
        divBtn.addActionListener(this);

        exitBtn = new Button("Quit");
        exitBtn.addActionListener(this);

        
        FlowLayout fl = new FlowLayout();

        frame.setLayout(fl);

        frame.add(lblNo1);
        frame.add(tfNo1);

        frame.add(lblNo2);
        frame.add(tfNo2);

        frame.add(lblAns);
        frame.add(tfAns);

        frame.add(addBtn);
        frame.add(subBtn);
        frame.add(mulBtn);
        frame.add(divBtn);
        frame.add(exitBtn);

        frame.setSize(600, 400);

        frame.setVisible(true);
        
        frame.dispatchEvent(new WindowEvent(frame, WindowEvent.WINDOW_CLOSING));


    }

    // Class without new??

    public void actionPerformed(ActionEvent e){

        int no1 = Integer.parseInt(tfNo1.getText());
        int no2 = Integer.parseInt(tfNo2.getText());
        int ans = 0;

        if(tfNo1.getText() == ""){

            tfNo1.hasFocus();

        }else if(tfNo2.getText() == ""){

            tfNo2.hasFocus();

        }else{

            //Button clicked = e.getSource();

        if(e.getSource() == addBtn){

            ans = no1 + no2;

        }else if(e.getSource() == subBtn){

            ans = no1 - no2;

        }else if(e.getSource() == mulBtn){

            ans = no1 * no2;

        }else if(e.getSource() == divBtn){

            ans = no1 / no2;
        }else if(e.getSource() == exitBtn){

            System.exit(0);
        }

        }

        tfAns.setText(Integer.toString(ans));

    }

    public static void main(String[] a){
        BasicCalculator c1 = new BasicCalculator();
    }
}