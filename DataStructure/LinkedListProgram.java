import java.util.Scanner;

class Node{
    public int data;
    public Node next;
}

class LinkedList{

    private Scanner cin = new Scanner(System.in);

    private Node head;

    public void add(int inputData){

        Node newNode = new Node();

        newNode.data = inputData;

        if(this.head == null){
            head = newNode;
            newNode.next = null;
            
        }else{

            Node traverser = new Node();
            traverser = head;

            while(traverser.next != null){
                traverser = traverser.next;
            }

            traverser.next = newNode;
        }

    }

    public int remove(){

        if(this.head == null){
            System.out.println("List is empty!!");
            return -1;
        }
        
        if(this.head.next == null){
                int value = this.head.data;
                this.head = this.head.next;
                return value;
        }else{

            Node traverser = new Node();
            Node prev = null;

            traverser = this.head;

            while(traverser.next != null){
                prev = traverser;
                traverser = traverser.next;
            }

            int data = traverser.data;

            prev.next = traverser.next;

            return data;
        }
}

    public void display(){

        if(this.head == null){
            System.out.println("/0\\");
        }

        Node traverser = new Node();

        traverser = head;
        while(traverser != null){
            System.out.print(traverser.data + "-->");
            traverser = traverser.next;
        }
        System.out.println("/0\\");
    }

}

class LinkedListProgram{

    public static void main(String[] args){

        LinkedList myList = new LinkedList();
        
        myList.add(1);
        myList.display();
        myList.add(2);
        myList.display();
        myList.add(3);
        myList.display();
        myList.add(4);
        myList.display();
        myList.add(5);
        myList.display();

        System.out.println(myList.remove());
        System.out.println(myList.remove());
        System.out.println(myList.remove());
        System.out.println(myList.remove());

        System.out.println(myList.remove());

        myList.add(1);
        myList.display();
        myList.add(2);
        myList.display();
        myList.add(3);
        myList.display();
        myList.add(4);
        myList.display();
        myList.add(5);
        myList.display();

    }
}
